import functools
import logging
import sys

import aiohttp.web
import click

import asyncio_server.web
import utils.path



option = functools.partial(click.option, show_default = True, required = True)


@click.command()
@click.argument(
	'root-directory',
	required = True,
	type = click.Path(exists = True, file_okay = False, dir_okay = True)
)
@option(
	'--bind-address',
	type = str,
	default = '127.0.0.1',
)
@option(
	'--bind-port',
	type = int,
	default = 8000
)
def main(root_directory: str, bind_address: str, bind_port: int) -> None:
	logging.basicConfig(
		level = logging.DEBUG,
		handlers = [logging.StreamHandler(sys.stderr)],
	)
	logger = logging.getLogger('__main__')

	app = aiohttp.web.Application()
	app.add_routes(asyncio_server.web.routes)

	with utils.path.cwd(root_directory):
		try:
			aiohttp.web.run_app(app, host = bind_address, port = bind_port)
		except:
			logger.exception('Unhandled exception')
			raise
		finally:
			logger.info('Shutting down.')



if __name__ == '__main__':
	main()
