import os
import glob
import textwrap
import urllib.parse

import aiohttp.web



routes = aiohttp.web.RouteTableDef()


@routes.get('/')
async def list_directory(_: aiohttp.web.Request) -> aiohttp.web.Response:
	# This is for example only - in real life use something more robust like Jinja2
	# Which will handle escaping and other things
	files = (
		(f, urllib.parse.quote_plus(f))
		for f in glob.iglob('*')
		if not (set(f) & set('<>'))
	)

	links = '\n'.join([
		f'<a href="{link}">{title}</a><br />'
		for title, link in files
	])

	return aiohttp.web.Response(
		body = textwrap.dedent(f'''
			<html>
				<head></head>
				<body>
					{links}
				</body>
			</html>
		'''),
		content_type = 'text/html',
	)


@routes.get('/{path}')
async def download_file(request: aiohttp.web.Request) -> aiohttp.web.StreamResponse:
	path = request.match_info['path']

	file_name = os.path.abspath(os.path.join(os.getcwd(), path))
	if not file_name.startswith(os.getcwd()):
		raise aiohttp.web.HTTPBadRequest(body = f'File = "{path}" is not in allowed path.')

	if not os.path.exists(file_name):
		raise aiohttp.web.HTTPNotFound(body = f'File = "{path}" not found.')

	response = aiohttp.web.StreamResponse(headers = {'content-type': 'application/octet-stream'})
	await response.prepare(request)

	# Tested with aiofile but SEGFAULT:)
	with open(file_name, 'rb') as f:
		while 1:
			buffer = f.read(20 * 16 * 1024)
			if not buffer:
				break

			if not buffer:
				break

			await response.write(buffer)


	await response.write_eof()
	return response
