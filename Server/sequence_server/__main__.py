import http.server
import socketserver
import functools
import logging
import sys

import click

import utils.path


server_types = {
	cls.__name__: cls
	for cls in (socketserver.TCPServer, socketserver.ThreadingTCPServer, socketserver.ForkingTCPServer)
}

option = functools.partial(click.option, show_default = True, required = True)


@click.command()
@click.argument(
	'root-directory',
	required = True,
	type = click.Path(exists = True, file_okay = False, dir_okay = True)
)
@option(
	'--server-type',
	type = click.Choice(server_types.keys()),
	default = next(iter(server_types.keys())),
)
@option(
	'--bind-address',
	type = str,
	default = '127.0.0.1',
)
@option(
	'--bind-port',
	type = int,
	default = 8000
)
def main(root_directory: str, server_type: str, bind_address: str, bind_port: int) -> None:
	logging.basicConfig(
		level = logging.DEBUG,
		handlers = [logging.StreamHandler(sys.stderr)],
	)
	logger = logging.getLogger('__main__')

	Server = server_types[server_type]

	# SimpleHTTPRequestHandler by default use current working directory to list files
	with utils.path.cwd(root_directory):
		with Server((bind_address, bind_port), http.server.SimpleHTTPRequestHandler) as server:
			logger.info('Listening on http://%s:%s/', bind_address, bind_port)

			try:
				server.serve_forever()
			except KeyboardInterrupt:
				# Just ignore and cleanup in finally block
				pass
			except:
				logger.exception('Unhandled exception')
				raise
			finally:
				logger.info('Shutting down.')


if __name__ == '__main__':
	main()
