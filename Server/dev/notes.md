Read data using curl. 8 terminals was good test case.

```
curl http://localhost:8000/bigBuckBunny.ascii.bz2 | bzip2 -d
```

Apache bench

```
ab -m GET -n 100 -c 50 http://localhost:8000/bigBuckBunny.ascii.bz2
```
