import contextlib
import os



@contextlib.contextmanager
def cwd(path: str) -> None:
	'''
	Change working directory and revert last one after context is left
	'''
	current = os.getcwd()
	try:
		os.chdir(path)
		yield
	finally:
		os.chdir(current)
