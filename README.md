# PYTHON ADVANCED

This repository contains all materials and slides used on [talk](http://www.datascript.cz/morning-talks/python-advanced-zpracovani-dat-generatory-vlakna/)
given for DataScript. In this talk we showed how GIL (Globlal intepretter lock) can change perofrmance
 of our Python application.

Also we got through basics of [asyncio](https://docs.python.org/3.6/library/asyncio.html):

   - how to use [aiohttp](https://aiohttp.readthedocs.io/en/stable/) to implement HTTP server and client,
   - how how to stream data

and Python [generators](https://wiki.python.org/moin/Generators).

All those exercises were wrapped around streaming ASCII art encoded movie [Big Buck Bunny](https://peach.blender.org/about/).
Which is also versioned in this repository.

[img2txt](https://github.com/hit9/img2txt) was used to convert each video frame to ASCII art. Learn more about video conversion in `convert/README.md`.

## Requirements

If you want to try code in this repo your self please follow these instructions:

  - Have CPython 3.6 installed (all examples will work with CPython 3.5 after removing [f-strings](https://www.python.org/dev/peps/pep-0498/))
  - This repository contains 3 subfolders with code examples and one `slides` with all slided and code used to generate it
    - Server
    - NotebookExamples
    - Player

  - All these directories contains it's own `requirements.txt` file which needs to be installed before
  code inside may be run. I recommend to prepare separated [virtual environments](https://virtualenv.pypa.io/en/stable/)
  and install those requirements to these
  - This repository also contains Git tag `version/1.0.0` where everything is already implemented
  this tag is used as reference solution.
