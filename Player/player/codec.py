from typing import NamedTuple, AsyncIterator, List # NOQA

from player.utils import aenumerate
from player.stream_file import StreamFile



Metadata = NamedTuple('Metadata', [
	('frame_rate', int),
	('frames_count', int),
	('line_length', int),
	('frame_height', int),
])


def _parse_header(bytes_data: bytes) -> Metadata:
	'''
	Parse line which contains header to object representation
	Headeline looks following:
		'+<frame rate>#<number of frames>#<line length>#<frame height>;'
	all numbers are fixed length of 10 digits
	'''
	data = bytes_data.decode()
	assert data[0] == '+' and data[-1] == ';', 'Wrong metadata header'
	return Metadata(*[int(field) for field in data[1:-1].split('#')])


def _is_header_line(data: bytes) -> bool:
	'''
	Check whether data looks like metadata line
	'''
	return data.startswith(b'+') and data.endswith(b';') and b'#' in data


async def get_file_metadata(file: StreamFile) -> Metadata:
	'''
	Read metadata from file created with convert
	this call will update file handler position right after the metadata
	'''
	metadata = _parse_header(await file.read(45))
	assert await file.read(1) == b'\n', 'We should have finish reading line with metadata'
	return metadata


async def get_frames(file: StreamFile, metadata: Metadata) -> AsyncIterator[bytes]:
	frame = [] # type: List[bytes]
	async for line_number, line in aenumerate(file):
		# Skip line with metadata
		if _is_header_line(line):
			continue

		if line_number % (metadata.frame_height + 1) == 0 and line_number > 0:
			yield b'\n'.join(frame)
			frame.clear()
			continue

		frame.append(line)
