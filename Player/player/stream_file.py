from typing import AsyncIterator



class StreamFile(AsyncIterator[bytes]):
	'''
	Implement really simple file like API over async iterable (stream) object
	'''

	def __init__(self, stream: AsyncIterator[bytes]) -> None:
		# TODO Implement
		pass


	async def read(self, size: int = 16 * 1024) -> bytes:
		# TODO Implement
		pass


	async def readline(self, separator = b'\n') -> bytes:
		# TODO Implement
		pass


	async def __aiter__(self) -> AsyncIterator[bytes]:
		return self


	async def __anext__(self) -> bytes:
		while True:
			buffer = await self.readline()
			if not buffer:
				raise StopAsyncIteration()

			return buffer
