from typing import AsyncIterator, Any, Tuple


async def aenumerate(stream: AsyncIterator[Any]) -> AsyncIterator[Tuple[int, Any]]:
	# TODO Implement
	pass


async def identity(stream: AsyncIterator[Any]) -> AsyncIterator[Any]:
	'''
	Used only for more readable syntax, does not change stream at all
	'''
	async for chunk in stream:
		yield chunk
