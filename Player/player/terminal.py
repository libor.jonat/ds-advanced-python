
# Move cursor to required position in terminal
MOVE_CURSOR_FORMAT = '\033[%d;%dH'

# Reset cursor to 0,0
RESET_CURSOR = MOVE_CURSOR_FORMAT % (0, 0)

# Clear all lines in terminal
CLEAR = '\x1b[2J'

# First white all empty lines
# And then reset cursor to upper left
CLEAR_SCREEN = CLEAR + RESET_CURSOR
