import asyncio
import logging
import sys
import contextlib

import click



async def play(url: str) -> None:
	'''
	Read data from remote HTTP url and play them locally in terminal
	'''
	# TODO Implement
	pass


@click.command()
@click.argument('url', required = True, type = str)
def main(url: str) -> None:
	logging.basicConfig(level = logging.DEBUG, handlers = [logging.StreamHandler(sys.stderr)])
	logger = logging.getLogger('__main__')

	loop = asyncio.get_event_loop()
	play_task = loop.create_task(play(url))
	try:
		loop.run_until_complete(play_task)
	except KeyboardInterrupt:
		logger.warning('Terminating..')
	except:
		logger.exception('Unhandled exception')
		raise
	finally:
		if not play_task.done():
			logger.warning('Stopping playing in progress')
			play_task.cancel()

			# Give some time to task to cancel
			with contextlib.suppress(asyncio.TimeoutError, asyncio.CancelledError):
				loop.run_until_complete(asyncio.wait_for(play_task, timeout = 0.3))

		loop.run_until_complete(loop.shutdown_asyncgens())
		loop.close()
		logger.info('Shut down')


if __name__ == '__main__':
	main()
