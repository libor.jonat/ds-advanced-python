from typing import AsyncIterator, Tuple
import bz2

import aiohttp

import player.stream_file
import player.codec



async def read_data(path: str, chunk_size: int = 16 * 1024) -> AsyncIterator[bytes]:
	'''
	Read streaming data from HTTP endpoint and propagate them as async generator
	'''
	async with aiohttp.ClientSession() as session:
		async with session.get(path) as response:
			while True:

				chunk = await response.content.read(chunk_size)
				if not chunk:
					break

				yield chunk


async def decompress(stream: AsyncIterator[bytes]) -> AsyncIterator[bytes]:
	'''
	Decompress stream data which contains multiple streams of BZ2
	'''
	BUFFER_SIZE = 8 * 1024

	# File like access helping here because we can read arbitrary chunk size
	# and we don't rely on what input stream give us
	file = player.stream_file.StreamFile(stream = stream)
	decompressor = bz2.BZ2Decompressor()

	while not file.stream_end:
		# Once we ended reading one stream jump to the new one
		if decompressor.eof:
			# Don't forget to read unused data from previous stream and pass them to the new one
			rawblock = (
				decompressor.unused_data or
				await file.read(BUFFER_SIZE)
			)
			decompressor = bz2.BZ2Decompressor()
		else:
			rawblock = await file.read(BUFFER_SIZE)

		data = decompressor.decompress(rawblock)
		if not data:
			continue

		yield data


async def read_data(path: str, chunk_size: int = 16 * 1024) -> AsyncIterator[bytes]:
	'''
	Read streaming data from HTTP endpoint and propagate them as async generator
	'''
	# TODO Implement
	pass


async def get_frames(stream: AsyncIterator[bytes]) -> AsyncIterator[Tuple[float, str]]:
	'''
	From plain bytes build frames (single video frames) and yield them
	Also read time between frames from metadata
	'''
	# TODO Implement
	pass


async def buffer_data(stream: AsyncIterator[bytes], *, size: int) -> AsyncIterator[bytes]:
	'''
	Buffer input stream so even if source of this stream is disconnected
	we still have some data in our pipeline to process

	:param size: defines how many chunks are stored - beware it does not take
		chunk size it self into decision
	'''
	# TODO Implement
	pass
