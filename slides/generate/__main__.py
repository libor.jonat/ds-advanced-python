from typing import Dict, Any
import sys
import yaml
import copy
import os
import requests

import jinja2


def get_image(path):
	if 'http://' in path or 'https://' in path:
		basename = os.path.basename(path)
		if '.' not in basename:
			basename = '{}.jpg'.format(basename)
		download_path = os.path.join(
			os.path.dirname(__file__),
			'..',
			'img',
			'download',
			basename
		)
		if not os.path.exists(download_path):
			r = requests.get(path, stream = True)
			if r.status_code == 200:
				with open(download_path, 'wb') as f:
					for chunk in r.iter_content(1024):
						f.write(chunk)

		return 'img/download/{}'.format(basename)
	else:
		return 'img/kubernetes/{}'.format(path)


def render_slides(data: Dict[str, Any]):
	env = jinja2.Environment(
		loader = jinja2.PackageLoader('generate', 'templates'),
		autoescape = jinja2.select_autoescape(['html', 'xml']),
		extensions = ['jinja2.ext.do'],
	)

	template = env.get_template('template.html.j2')
	print(template.render(**data))


def process_slides(category):
	for slide in category['slides']:
		if 'image' in slide:
			slide['image'] = get_image(slide['image'])

		if 'uml' in slide:
			slide['content'] = ['TODO UML will be added']

		if 'highlight' in slide:
			highlight = slide['highlight']
			if not isinstance(highlight, list):
				highlight = [highlight]

			lines = slide['code'].split('\n')
			try:
				if slide['highlight-split']:
					yield copy.deepcopy(slide)
			except KeyError:
				slide['highlight-split'] = False

			for h in highlight:
				if isinstance(h, int):
					f, t = h, h
				else:
					f, t = map(int, map(lambda v: v.strip(), h.split('-')))

				if slide['highlight-split']:
					lines = slide['code'].split('\n')

				for index in range(f, t + 1):
					code = lines[index]
					spaces = len(code) - len(code.lstrip())
					code = code.strip()

					lines[index] = (' ' * spaces) + '<mark>' + code + '</mark>'

				if slide['highlight-split']:
					s = copy.deepcopy(slide)
					s['code'] = '\n'.join(lines)
					yield s

			if slide['highlight-split']:
				continue
			slide['code'] = '\n'.join(lines)

		yield slide


def process_presentation(data):
	for required in ('categories', 'slides', 'about', 'notes',):
		if required not in data:
			data[required] = []

	for category in data['categories']:
		category['image'] = get_image(category['image'])

		if 'slides' not in category:
			category['slides'] = []

		category['slides'].insert(0, {
			'category-slide': True,
			'title': category['name'],
			'head_image': category['image'],
			'notes': category.get('notes', []),
			'title_css': category.get('title_css')
		})
		category['slides'] = list(process_slides(category))

	return data


def main(input_file: str) -> None:
	with open(input_file) as f:
		slides_definition = yaml.safe_load(f)

	render_slides(process_presentation(slides_definition))



if __name__ == '__main__':
	main(sys.argv[1])
